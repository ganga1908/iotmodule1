# IoT Basics
### What is IoT?

Internet of Things (IoT) is an ecosystem of `connected physical objects` that are accessible through the internet.

The embedded technology in the objects helps them to interact with internal states or the external environment, which in turn affects the decisions taken.

### Why IoT?

IoT enables devices/objects to observe, identify and understand a situation or the surroundings without being dependent on human help.

### What is the scope of IoT?

Home IoT - Garage IoT - Businesses IoT

Name it! Every device around us can be connected to the internet, making life simpler.

The only question remaining being: *Is it really neccessary?*

Industrial IoT is one such subsection.
### What is IIoT?

Industrial IoT (IIoT) refers to the application of IoT technology in `industrial settings`, especially with respect to instrumentation and control of sensors and devices that engage `cloud technologies`. 

### How it all began...
**Industry 1.0 - 1784** - Mechanization, Steam Power, Weaving loom

**Industry 2.0 - 1870** - Mass production, Assembly lines, Electrical energy

**Industry 3.0 - 1969** - Automation, Computers and Electronics(IT)

**Industry 4.0 - Today** - Cyber Physical Systems, Internet of Things, Network

### Industry 3.0 

Take a look at the pyramid given below for a Bird's-eye view

![Pyramid](/extras/pyramid.png)

Field devices are those that are `physically present` in the industry setting, and measure values:
 - Sensors
 - Actuators
 - RTUs(Remote Terminal Units)

Control devices comprise of:
 - PCs 
 - PLCs(Programmable Logic Controller)
 - CNCs(Computer Numerical Control)
 - DCS(Distributed Control System)

Set of control devices form `Stations`, which form `Work Centers`.

`Enterprise` consitutes of multiple work centers.


### Typical Industry 3.0 Architecture
![Architecture](/extras/architecture3.0.png)

Field devices communicate with Control devices using Field Buses.

Data flow can be simplified as follows:

Field Devices --> Control Devices --> SCADA/HISTORIAN(Work Centers) --> ERP/MES(Enterprise)

The data gets stored in databases as CSVs etc., rarely plotted as real-time graphs or charts.

### Industry 3.0 Communication protocols

These protocols are optimized for data delivery to the central server in the factory.

 - MODBUS
 - PROFINET
 - CANopen
 - EtherCAT

***Industry 4.0 is basically Industry 3.0 connected to the internet.***

### Operations on the collected data
 - Dashboard
 - Remote Web SCADA
 - Remote control configuration of devices 
 - Predictive Maintenance 
 - Real time event stream processing
 - Analytics with predictive models
 - Automated device provisioning, Auto Discovery
 - Real time alerts and alarms
### Typical Industry 4.0 Architecture

![Architecture](/extras/architecture4.0.png)

`Edge` gets the data from controllers and convert it to a cloud understandable protocol

### Industry 4.0 communication protocols
 - MQTT
 - AMQP
 - CoAP
 - OPC UA
 - Websockets
 - HTTP
 - REST API
### Problems with Industry 4.0 upgrades
 1. Cost
 2. Downtime
 3. Reliability
### Solution

Get data from Industry 3.0 devices/meters/sensors without changes to the original device.

Send the data to the Cloud using Industry 4.0 devices

![Conversion](/extras/conversion.png)

### Challenges in coversion
 - Expensive hardware
 - Lack of documentation
 - Proprietary PLC protocols

### Roadmap to making your own Industrial IoT

**Step 1:** Identify most popular Industry 3.0 devices
**Step 2:** Study protocols that these devices communicate
**Step 3:** Get data from Industry 3.0 devices
**Step 4:** Send the data to cloud for Industry 4.0
### Tweaks made

 - Data is stored in TSDBs - Time series databases - e.g., Prometheus, InfluxDB
 - IoT Dashboards - For visualisation - e.g., Grafana, Thingsboard
 - IoT Platforms - Provides other features - e.g., AWS IoT, Google IoT, Azure IoT, Thingsboard
 - Get alerts - e.g., Zaiper, Twilio
### Summary

![Summary](/extras/summary.png)

**What happens if the internet connection is not reliable?**

MQTT, AMQP, CoAP, OPC UA have inbuilt data collection centers i.e., the data going to be sent is queued/buffered.

Minimal risk of data leaks and other security issues

*Most importantly, take note of interfaces and protocols for any given Industry 3.0 device before proceeding with the conversion*


